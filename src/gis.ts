const radiusEarthKm = 6371 // earthRadius = 6371008.8 maplibre-gl/geo/lng_lat

const degToRad = Math.PI / 180

export const lengthKm = (coords: [number, number][]): number => {
    // Returns total length in km from array of coordinates
    // Haversine formula [https://en.wikipedia.org/wiki/Haversine_formula], noting atan2(sqrt(x), sqrt(1-x)) = asin(sqrt(x)) [https://github.com/Turfjs/turf]
    var d = 0
    var [prevLon, prevLat] = coords[0]
    prevLon *= degToRad
    prevLat *= degToRad
    coords.forEach(([lon, lat]) => {
        const h = Math.sin((prevLat - (lat *= degToRad)) / 2) ** 2 + Math.sin((prevLon - (prevLon = lon * degToRad)) / 2) ** 2 * Math.cos(prevLat) * Math.cos(prevLat = lat)
        d += Math.atan2(Math.sqrt(h), Math.sqrt(1 - h))
    })
    return 2 * radiusEarthKm * d
}