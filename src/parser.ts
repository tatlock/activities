import { GeoJSONFeature, EncodedFeature, EncodedUntypedFeature } from './types'

/**
 * Decode bytes to GeoJSON
 * 
 * @param arr - An 8-bit unsigned integer array of bytes
 * @returns Decoded GeoJSON containing FeatureCollection of LineStrings
 */
export function decodeToGeoJSON(arr: Uint8Array): string {
    return JSON.stringify({
        type: 'FeatureCollection',
        features: decodeToGeoJSONFeatures(arr)
    })
}

/**
 * Encode GeoJSON to bytes
 * 
 * @param geojson - GeoJSON containing FeatureCollection of LineStrings
 * @param precision - Precision used to encode coordinates
 * @returns An 8-bit unsigned integer array of encoded bytes
 */
export function encodeFromGeoJSON(geojson: string, precision: number = 16): Uint8Array {
    return encodeFromGeoJSONFeatures(JSON.parse(geojson).features, precision)
}

/**
 * Encode object of GeoJSON FeatureCollection of LineStrings to bytes
 * 
 * @param arr - An 8-bit unsigned integer array of bytes
 * @param precision - Precision used to encode coordinates
 * @returns An 8-bit unsigned integer array of encoded bytes
 */
function encodeFromGeoJSONFeatures(features: GeoJSONFeature[], precision: number): Uint8Array {
    const featureTypes = features.reduce((a: { [key: string]: EncodedFeature[] }, v: GeoJSONFeature) => {
        const props = v.properties
        const t = props.type
        const d = props.date
        var x = a[t]
        if (!x) { x = a[t] = [] }
        x.push({ ...v, properties: { ...props, date: new Date(d).getTime() / 6e4 } })
        return a
    }, {})
    const partialEncodes = Object.entries(featureTypes).map(([type, x], i) => {
        const activities = new ActivityFeatures()
        activities.partialInit(precision)
        var buf, _
        for (let j = 0; j < x.length; j++) {
            [buf, _] = activities.addFeature(x[j])
        }
        const sb = activities.getScaleBuf()
        const ps = [new TextEncoder().encode(type + '\0'), buf.slice(sb.length)]
        if (!i) ps.unshift(sb)
        return ps
    }).flat(1)
    const enc = new Uint8Array(partialEncodes.reduce(accumLen, 0))
    var cur = 0
    partialEncodes.forEach(x => {
        enc.set(x, cur)
        cur += x.length
    })
    return enc
}

/**
 * Decode bytes to object of GeoJSON FeatureCollection of LineStrings.
 * 
 * @param arr - An 8-bit unsigned integer array of bytes
 * @returns An object representing the decoded GeoJSON
 */
function decodeToGeoJSONFeatures(arr: Uint8Array): GeoJSONFeature[] {
    const data = new ActivityFeatures(arr)
    const features = []
    for (let [buf, type] of data.typeSlicer()) {
        const activities = new ActivityFeatures(buf)
        activities.decode().forEach(f => {
            const { properties } = f
            features.push({
                ...f, properties: {
                    ...properties,
                    type,
                    date: new Date(f.properties.date * 6e4)
                }
            })
        })
    }
    return features
}

/**
 * Encode a non-negative integer number using variable length integer encoding.
 * 
 * The number is encoded into 7 least significant bits of each byte, with the
 * most significant bit (MSB) indicating continuation. When the MSB is 1, the
 * encoding continues into the next bit. When the MSB is 0 the byte contains
 * the 7 least significat bits of the input number.
 * 
 * @param x - The number to be encoded
 * @returns An array of 8-bit numbers definining the encoded bytes
 */
function encodeVarUint(x: number): number[] {
    const a = [x & 127]
    x = Math.floor(x / 128)
    while (x > 0) {
        a.unshift(x & 127 | 128)
        x = Math.floor(x / 128)
    }
    return a
}

/**
 * Encode a doubled integer number using variable length signed integer encoding.
 * 
 * The number is first encoded using zigzag encoding, where positive numbers
 * are encoded 
 * 
 * @param x - The number to be encoded
 * @returns An array of 8-bit numbers definining the encoded bytes
 */
function encodeVarSintDoubled(x: number): number[] {
    if (x < 0) x = -1 - x
    return encodeVarUint(x)
}

/**
 * Encode feature properties.
 * 
 * @param p - The properties to be encoded
 * @returns An array of 8-bit numbers definining the encoded bytes
 */
function encodeProps(p: { stravaActivityId?: number }): Uint8Array {
    const stravaActivityId = p.stravaActivityId
    if (stravaActivityId === undefined) {
        return new Uint8Array(encodeVarUint(0))
    }
    const t = encodeVarUint(1)
    return new Uint8Array(t.concat(...encodeVarUint(stravaActivityId)))
}

/**
 * Simplify coordinates.
 * 
 * Simplfies coordinates by rounding to the nearest integer and using the
 * Ramer–Douglas–Peucker algorithm to simplify the line.
 * 
 * @param coords - An array of coordinates
 * @returns An array containing the simplified coordinates and a boolean mask
 */
function coordsSimplify(coords: [number, number][]): [[number, number][], boolean[]] {
    const { length } = coords
    const roundedCoords = coords.map((x: [number, number]): [number, number] => x.map(Math.round))
    const mask = Array(length).fill(true)
    const stack = [[0, length - 1]]
    var maxIndex: number
    var p: number
    var q: number
    while (stack.length) {
        const [startIndex, endIndex] = stack.pop()
        const nextIndex = startIndex + 1
        const [lx, ly] = roundedCoords[startIndex]
        const [mx, my] = roundedCoords[endIndex]
        const lineDX = mx - lx
        const lineDY = my - ly
        if (nextIndex >= endIndex) continue
        var maxDist = 0
        for (var i = nextIndex; i < endIndex; i++) {
            const [px, py] = coords[i]
            const pDX = px - lx
            const pDY = py - ly
            const toStart = lineDX * pDX + lineDY * pDY
            if (toStart <= 0) {
                p = lx
                q = ly
            } else {
                const lenSq = lineDX ** 2 + lineDY ** 2
                if (lenSq <= toStart) {
                    p = mx
                    q = my
                } else {
                    const r = toStart / lenSq
                    p = lx + r * lineDX
                    q = ly + r * lineDY
                }
            }
            // Calculate perpendicular distance from point to line
            const distance = (p - px) ** 2 + (q - py) ** 2
            if (distance > maxDist) {
                maxIndex = i
                maxDist = distance
            }
        }
        if (maxDist > 0.25) {
            stack.push([startIndex, maxIndex], [maxIndex, endIndex])
        } else {
            mask.fill(false, nextIndex, endIndex)
        }
    }
    return [roundedCoords.filter((_, i) => mask[i]), mask]
}

/**
 * Encode simplified coordinates.
 * 
 * @param coords - The coordinates to be encoded ([longitude, latitude])
 * @param scaleFactor - The scaling for encoding and line simplification
 * @param lat - The latitude to encode relative to
 * @param lon - The longitude to encode relative to
 * @returns An array containing an array of 8-bit encoded numbers, the end
 * latitude, end longitude and the mask applied to simplify the coordinates
 */
function encodeCoords(
    coords: [number, number][],
    scaleFactor: number,
    lat: number,
    lon: number
): [number[], number, number, boolean[]] {
    const encCoords = []
    const lons = []
    const scaledCoords = coords.map(([y, x]): [number, number] => {
        const ln = y * Math.cos(x * Math.PI / 180) / scaleFactor
        const lt = x / scaleFactor
        return [ln, lt]
    })
    const [simplifiedCoords, coordMask] = coordsSimplify(scaledCoords)
    simplifiedCoords.forEach(([ln, lt]) => {
        encCoords.push((lt *= 2) - lat)
        lons.push((ln *= 2) - lon)
        lat = lt
        lon = ln
    })
    encCoords.splice(1, 0, lons.shift())
    return [
        encodeVarUint(simplifiedCoords.length).concat(...encCoords.concat(lons).map(encodeVarSintDoubled)),
        lat,
        lon,
        coordMask
    ]
}

const accumLen = (a: number, v: []): number => a + v.length

export class ActivityFeatures {

    #buf
    #cursor
    #latDoubled
    #lonDoubled
    #date
    #props
    #dates
    #endLtLn
    #data
    #activityNames
    #cSF
    #coordScaleFactor;
    #textDecoder = new TextDecoder();
    #textEncoder = new TextEncoder();

    constructor(buf?: Uint8Array) {
        this.#buf = buf
        this.#cursor = 0;
        this.#latDoubled = 0;
        this.#lonDoubled = 0;
        this.#date = 0;
    }

    #coordDecode(lt: number, ln: number): [number, number] {
        lt *= this.#coordScaleFactor
        return [ln * this.#coordScaleFactor / Math.cos(lt * Math.PI / 180), lt]
    }

    #nextVarInt(): number {
        var a: number, x = 0
        while ((a = this.#buf[this.#cursor++]) & 128) x = (x + (a & 127)) * 128
        return x + a
    }

    #nextString(): string {
        // NULL terminated strings
        const s = this.#textDecoder.decode(this.#buf.slice(this.#cursor, this.#cursor = this.#buf.indexOf(0, this.#cursor)))
        this.#cursor++
        return s
    }

    #nextVarSintDoubled(): number {
        const x = this.#nextVarInt()
        return -x & 1 ? -x - 1 : x
    }

    #nextActivity(properties: { name: string, date?: number, stravaActivityId?: number }): EncodedUntypedFeature {
        const t = this.#nextVarInt()
        if (t === 1) {
            properties.stravaActivityId = this.#nextVarInt()
        }
        const date = this.#date += this.#nextVarInt()
        const numCoords = this.#nextVarInt()
        const startPoint = this.#coordDecode(
            this.#latDoubled += this.#nextVarSintDoubled(),
            this.#lonDoubled += this.#nextVarSintDoubled()
        )
        const latsDoubled = Array.from({ length: numCoords - 1 }, x => {
            return this.#latDoubled += this.#nextVarSintDoubled()
        })
        const coords = latsDoubled.map(x => {
            return this.#coordDecode(x, this.#lonDoubled += this.#nextVarSintDoubled())
        })
        coords.unshift(startPoint)
        return {
            type: 'Feature',
            properties: { ...properties, date },
            geometry: {
                type: 'LineString',
                coordinates: coords
            }
        }
    }

    #skipVarUint() {
        while (this.#buf[this.#cursor++] & 128) { }
    }

    #countNames(): number {
        var numActivities = 0
        while (this.#cursor != (this.#cursor = this.#buf.indexOf(0, this.#cursor))) {
            numActivities++
            this.#cursor++
        }
        this.#cursor++
        return numActivities
    }

    #rescale(coordScaleFactor: number): Uint8Array {
        const s = this.#coordScaleFactor / coordScaleFactor
        const t = s - 1
        if (s < 1) {
            throw new RangeError('Downscaling coordinates not supported')
        }
        this.#data = this.#data.map(d => {
            this.#buf = d
            this.#cursor = 0
            const numCoords = 2 * this.#nextVarInt()
            const numCoordsEnd = this.#cursor
            const data = [...this.#buf.slice(0, numCoordsEnd)]
            for (var j = 0; j < numCoords; j++) {
                const x = this.#nextVarInt()
                data.push(...encodeVarUint(s * x + t * (x & 1)))
            }
            return data
        })
        return this.#partialEncode().slice(this.#cSF.length)
    }

    #partialEncode(): Uint8Array {
        var cur = this.#cSF.length
        const dates = this.#dates.map((x, i, a) => encodeVarUint(x - (a[i - 1] || 0)))
        const names = this.#textEncoder.encode(this.#activityNames.join('\0'))
        const len = this.#data.reduce(accumLen, dates.reduce(accumLen, this.#props.reduce(accumLen, names.length + 2 + cur)))
        const buf = new Uint8Array(len)
        buf.set(this.#cSF, 0)
        buf.set(names, cur)
        cur += 2 + names.length
        dates.forEach((d, i) => {
            const p = this.#props[i]
            const data = this.#data[i]
            buf.set(p, cur)
            cur += p.length
            buf.set(d, cur)
            cur += d.length
            buf.set(data, cur)
            cur += data.length
        })
        return buf
    }

    partialInit(coordScaleParam: number) {
        this.#coordScaleFactor = 2 ** (-coordScaleParam - 1)
        this.#cSF = encodeVarSintDoubled(2 * coordScaleParam)
        this.#props = []
        this.#dates = []
        this.#endLtLn = []
        this.#data = []
        this.#activityNames = []
    }

    partialDecode() {
        this.#coordScaleFactor = 2 ** (-this.#nextVarSintDoubled() / 2 - 1)
        this.#cSF = this.#buf.slice(0, this.#cursor)
        this.#props = []
        this.#dates = []
        this.#endLtLn = []
        this.#data = []
        this.#activityNames = []
        var lt = 0
        var ln = 0
        var s
        while ((s = this.#nextString()) !== '') {
            this.#activityNames.push(s)
        }

        this.#activityNames.forEach(() => {
            const cur = this.#cursor
            const t = this.#nextVarInt()
            if (t === 1) {
                this.#skipVarUint()
            }
            this.#props.push(this.#buf.slice(cur, this.#cursor))
            this.#dates.push(this.#date += this.#nextVarInt())
            const dataStart = this.#cursor

            const numCoords = this.#nextVarInt()
            lt += this.#nextVarSintDoubled()
            ln += this.#nextVarSintDoubled()
            for (var i = 1; i < numCoords; i++) {
                lt += this.#nextVarSintDoubled()
            }
            for (var i = 1; i < numCoords; i++) {
                ln += this.#nextVarSintDoubled()
            }
            this.#endLtLn.push([lt, ln])
            this.#data.push(this.#buf.slice(dataStart, this.#cursor))
        })
    }

    getScaleFactor(): number {
        return this.#coordScaleFactor
    }

    encodeSlice(type: string, coordScaleFactor: number): Uint8Array {
        const t = this.#textEncoder.encode(type + '\0')
        const buf = this.#coordScaleFactor === coordScaleFactor ? this.#buf.slice(this.#cSF.length) : this.#rescale(coordScaleFactor)
        return new Uint8Array([...t, ...buf])
    }

    addFeature(feature: EncodedFeature): [Uint8Array, boolean[]] {
        const props = feature.properties
        const date = props.date
        const len = this.#dates.length
        var idx = this.#dates.findIndex(d => d > date)
        if (idx === -1) idx = len
        const [prevLt, prevLn] = idx ? this.#endLtLn[idx - 1] : [0, 0]
        const geom = feature.geometry.coordinates
        if (geom.length < 2) { // Skip any features with insufficient points for a line
            return
        }
        const [data, newLt, newLn, coordMask] = encodeCoords(geom, 2 * this.#coordScaleFactor, prevLt, prevLn)
        if (idx < len) {
            this.#buf = this.#data[idx]
            this.#cursor = 0
            this.#skipVarUint()
            const numCoordsEnd = this.#cursor
            const nextLt = this.#nextVarSintDoubled() + prevLt - newLt
            const nextLn = this.#nextVarSintDoubled() + prevLn - newLn
            const newData = encodeVarSintDoubled(nextLt).concat(encodeVarSintDoubled(nextLn))
            const updatedBuf = this.#data[idx] = new Uint8Array(this.#buf.length + newData.length + numCoordsEnd - this.#cursor)
            updatedBuf.set(this.#buf.slice(0, numCoordsEnd), 0)
            updatedBuf.set(newData, numCoordsEnd)
            updatedBuf.set(this.#buf.slice(this.#cursor), numCoordsEnd + newData.length)
        }
        this.#data.splice(idx, 0, data)
        this.#endLtLn.splice(idx, 0, [newLt, newLn])
        this.#dates.splice(idx, 0, date)
        this.#props.splice(idx, 0, encodeProps(props))
        this.#activityNames.splice(idx, 0, props.name)
        return [this.#partialEncode(), coordMask]
    }

    getSize(): [number, number] {
        this.#skipVarUint()
        return [this.#countNames(), this.#buf.length]
    }

    getScaleBuf(): number[] {
        return this.#cSF
    }

    decode(): EncodedUntypedFeature[] {
        this.#coordScaleFactor = 2 ** (-this.#nextVarSintDoubled() / 2 - 1)
        var s
        const activityNames = []
        while ((s = this.#nextString()) !== '') {
            activityNames.push(s)
        }
        return activityNames.map(name => this.#nextActivity({ name }))
    }

    * typeSlicer(): Generator<[Uint8Array, string]> {
        const buflen = this.#buf.length
        this.#skipVarUint()
        const coordScaleBytes = this.#cursor
        while (this.#cursor < buflen) {
            const type = this.#nextString()
            const typeEncodeStart = this.#cursor - coordScaleBytes
            this.#buf.copyWithin(typeEncodeStart, 0, coordScaleBytes)
            const numActivities = this.#countNames()
            for (var i = 0; i < numActivities; i++) {
                const t = this.#nextVarInt()
                // Skip ID
                if (t === 1) {
                    this.#skipVarUint()
                }
                // Skip date
                this.#skipVarUint()
                // Skip coordinates
                const numCoords = 2 * this.#nextVarInt()
                for (var j = 0; j < numCoords; j++) {
                    this.#skipVarUint()
                }
            }
            yield [this.#buf.slice(typeEncodeStart, this.#cursor), type]
        }
    }
}
