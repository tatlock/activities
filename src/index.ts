import { heatmap, mapFetch, updateLayer, updateHeatmapScale } from './map';
import { getStravaActivities, stravaActivitiesByType } from './strava';
import { dBOnUpgrade } from './db';
import { ActivityFeatures } from './parser';
import { EncodedUntypedFeature } from './types';

const stravaActivities = getStravaActivities()
const fetchedMap = mapFetch()

navigator.serviceWorker?.register(new URL('./sw.ts', import.meta.url))

async function dataParser(type: string, data: Uint8Array): Promise<[string, EncodedUntypedFeature[]]> {
	const activities = new ActivityFeatures(data)
	const features = activities.decode()
	return [type, features]
}

const request = indexedDB.open('activities')

request.onsuccess = () => {
	const db = request.result
	const req = db.transaction('activities', 'readonly')
	const store = req.objectStore('activities')
	const activityRequest = store.openCursor()
	const fetchedData = []
	activityRequest.onsuccess = () => {
		const cur = activityRequest.result;
		if (cur) {
			const type = cur.key
			const data = cur.value
			fetchedData.push(dataParser(type, data))
			cur.continue()
		} else {
			fetchedMap.then(map => {
				const scaleMax = localStorage.getItem('activities-heatmap-scale') || "100"
				const heatmapScale = document.getElementById('hms')
				const heatmapButton = document.getElementById('hm')
				heatmapButton.addEventListener('click', heatmap(map))
				heatmapScale.addEventListener('input', updateHeatmapScale(map))
				if (scaleMax !== null) {
					heatmapScale.value = scaleMax
				}
				return Promise.all(fetchedData.map(d => {
					return d.then(([t, f]) => {
						return updateLayer(map, t, f).then(() => f.reduce((a: number[], v: EncodedUntypedFeature) => {
								const s = v.properties.stravaActivityId
								if (s) a.push(s)
								return a
							}, []))
					})
				})).then(x => stravaActivities.then(y => stravaActivitiesByType(y, map, new Set(x.flat()))))
			}).then((x) => {
				const spinner = document.getElementById("spinner")
				spinner.style.display = "none";
			})
		}
	}
}
request.onupgradeneeded = dBOnUpgrade
