import { dBOnUpgrade } from '../db';
import { ActivityFeatures } from '../parser';

const url = new URL(window.location.href);

function filesOpen(e) {
	const files = e.currentTarget.files;
	const p = []
	for (var i = 0; i < files.length; i++) {
		p.push(new Promise(resolve => {
			const file = files[i];
			const reader = new FileReader();
			reader.onload = () => {
				const r = reader.result
				const buf = new Uint8Array(r)
				const features = new ActivityFeatures(buf)
				const request = indexedDB.open('activities')
				request.onsuccess = (e) => {
					const db = e.target.result
					const req = db.transaction('activities', 'readwrite')
					const store = req.objectStore('activities')
					req.oncomplete = () => {
						resolve()
					}
					for (let x of features.typeSlicer()) {
						store.add(...x)
					}
				}
				request.onupgradeneeded = dBOnUpgrade
			}
			reader.readAsArrayBuffer(file);
		}))
	}
	Promise.all(p).then(() => {
		location.reload()
	})
}

function calculatePrecision(p) {
	// Approximate combined lat-lon precision through trigonometry
	const precision = 222639 * 2 ** (-.5 - p)
	const s = (precision > 1e3 ? 0 : precision < .01 ? 1 : precision < 1 ? 2 : 3)
	return `${(precision * [.001, 1e3, 1e2, 1][s]).toPrecision(3)} ${['k', 'm', 'c', ''][s]}m`
}

function precisionChange(e) {
	document.getElementById('precision-output').innerHTML = calculatePrecision(e.target.value)
}

function removeData(storeName) {
	if (confirm('Delete all data?')) {
		const request = indexedDB.open('activities')
		if (storeName === 'strava') localStorage.removeItem('stravaUntilDate')
		request.onsuccess = (e) => {
			const db = e.target.result
			const req = db.transaction(['activities', 'strava'], 'readwrite')
			const store = req.objectStore(storeName)
			const clr = store.clear()
			clr.onsuccess = () => {
				location.reload()
			}
		}
		request.onupgradeneeded = dBOnUpgrade
	}
}

function closeSettings() {
	url.pathname = url.pathname.replace(/\/settings(\/(index.html)?)?$/, '')
	window.location.replace(url)
}

function linkedStorage(e) {
	localStorage.setItem(e.target.id, e.target.type === 'checkbox' ? e.target.checked : e.target.value)
}

function buildActivitiesTable() {
	const request = indexedDB.open('activities')
	request.onsuccess = (e) => {
		const db = e.target.result
		const req = db.transaction('activities', 'readonly')
		const store = req.objectStore('activities')
		const activityRequest = store.openCursor()
		const fetchedData = []
		activityRequest.onsuccess = (ev) => {
			const cur = ev.target.result;
			if (cur) {
				const type = cur.key
				const data = cur.value
				const activities = new ActivityFeatures(data)
				const [len, size] = activities.getSize()
				fetchedData.push([type, len, size])
				cur.continue()
			} else {
				const tbl = document.getElementById('activities-table')
				fetchedData.sort((a, b) => {
					const n = b[1] - a[1]
					if (n !== 0) return n
					const s = b[2] - a[2]
					if (s !== 0) return s
					return b[0] - a[0]
				})
				fetchedData.forEach((data) => {
					const row = tbl.insertRow(-1)
					data.forEach(c => {
						const cell = row.insertCell(-1)
						cell.innerHTML = c
					})
				})
			}
		}
	}
	request.onupgradeneeded = dBOnUpgrade
}

function exportData() {
	const request = indexedDB.open('activities')
	request.onsuccess = (e) => {
		const db = e.target.result
		const req = db.transaction('activities', 'readonly')
		const store = req.objectStore('activities')
		const activityRequest = store.openCursor()
		const features = []
		var minSFActivity, minScaleFactor = Infinity
		activityRequest.onsuccess = (ev) => {
			const cur = ev.target.result;
			if (cur) {
				const type = cur.key
				const data = cur.value
				const activities = new ActivityFeatures(data)
				activities.partialDecode()
				const coordScaleFactor = activities.getScaleFactor()
				if (coordScaleFactor < minScaleFactor) {
					minSFActivity = activities
					minScaleFactor = coordScaleFactor
				}
				features.push([type, activities])
				cur.continue()
			} else {
				const parts = [minSFActivity.getScaleBuf(), ...features.map(([t, a]) => a.encodeSlice(t, minScaleFactor))]
				const dl = document.createElement('a')
				dl.href = window.URL.createObjectURL(new Blob(parts, { type: 'application/octet-stream' }))
				dl.download = 'activities.act'
				dl.click()
			}
		}
	}
	request.onupgradeneeded = dBOnUpgrade
}

window.onload = () => {
	['strava-Client-ID', 'strava-Client-Secret', 'activities-Precision'].forEach(x => {
		const v = localStorage.getItem(x)
		if (v !== null) document.getElementById(x).value = v
	})
	const stravaAPIFields = document.getElementById('strava-Authentication')
	const stravaAPIDisabled = localStorage.getItem('strava-API-Disabled') === 'true'
	const stravaAPICheckbox = document.getElementById('strava-API-Disabled')
	const labelsOnTop = localStorage.getItem('activities-labels-on-top') !== 'false'
	const basemapStyle = localStorage.getItem('activities-style-id') || '3e1a00aeae81496587988075fe529f71'
	const labelsCheckbox = document.getElementById('activities-labels-on-top')
	const basemapSelect = document.getElementById('activities-style-id')
	basemapSelect.value = basemapStyle
	stravaAPIFields.disabled = stravaAPIDisabled
	stravaAPICheckbox.addEventListener('change', (e) => {
		stravaAPIFields.disabled = e.currentTarget.checked
	})
	labelsCheckbox.checked = labelsOnTop
	stravaAPICheckbox.checked = stravaAPIDisabled
	const form = document.getElementById('form')
	form.addEventListener('input', linkedStorage)

	const fileInput = document.getElementById('file-input')
	fileInput.addEventListener('change', filesOpen)
	document.getElementById('x').addEventListener('click', closeSettings)

	const precisionInput = document.getElementById('activities-Precision')
	precisionInput.addEventListener('input', linkedStorage)
	precisionInput.addEventListener('input', precisionChange)
	document.getElementById('precision-output').innerHTML = calculatePrecision(precisionInput.value)

	const importButton = document.getElementById('import')
	const exportButton = document.getElementById('export')
	const remove = document.getElementById('remove')
	const removeStrava = document.getElementById('remove-strava')
	importButton.addEventListener('click', () => document.getElementById('file-input').click())
	exportButton.addEventListener('click', exportData)
	remove.addEventListener('click', () => {
		removeData('activities')
	})
	removeStrava.addEventListener('click', () => {
		removeData('strava')
	})

	buildActivitiesTable()
}
