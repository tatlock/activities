type FeatureProperties = { name: string, date: Date, type: string, stravaActivityId?: number }

type EncodedFeatureProperties = Omit<FeatureProperties, 'date'> & { date: number }

export type GeoJSONFeature = {
    type: 'Feature',
    properties: FeatureProperties,
    geometry: { type: 'LineString', coordinates: [number, number][] }
}

export type EncodedFeature = Omit<GeoJSONFeature, 'properties'> & {
    properties: EncodedFeatureProperties
}

export type EncodedUntypedFeature = Omit<GeoJSONFeature, 'properties'> & {
    properties: Omit<EncodedFeatureProperties, 'type'>
}