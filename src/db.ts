
export function dBOnUpgrade(e) {
	e.target.result.createObjectStore('activities')
	e.target.result.createObjectStore('strava')
}

export async function getStoreKey(name, store, key) {
	return new Promise(resolve => {
		const request = indexedDB.open(name)
		request.onsuccess = (e) => {
			const db = e.target.result
			const req = db.transaction(store, 'readonly')
			const objStore = req.objectStore(store)
			objStore.get(key).onsuccess = (ev) => {
				resolve(ev.target.result)
				
			}
		}
		request.onupgradeneeded = dBOnUpgrade
	})
}

export async function setStoreKey(name, store, key, data) {
	return new Promise(resolve => {
		const request = indexedDB.open(name)
		request.onsuccess = (e) => {
			const db = e.target.result
			const req = db.transaction(store, 'readwrite')
			const objStore = req.objectStore(store)
			objStore.put(data, key).onsuccess = () => {
				resolve()
			}
		}
		request.onupgradeneeded = dBOnUpgrade
	})
}

export async function delStoreKey(name, store, key) {
	return new Promise(resolve => {
		const request = indexedDB.open(name)
		request.onsuccess = (e) => {
			const db = e.target.result
			const req = db.transaction(store, 'readwrite')
			const objStore = req.objectStore(store)
			objStore.delete(key).onsuccess = () => {
				resolve()
			}
		}
		request.onupgradeneeded = dBOnUpgrade
	})
}