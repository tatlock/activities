self.addEventListener('install', e => {
    e.waitUntil(
        caches.open('activities-map').then(cache => cache.addAll([
            './index.html',
            './settings/index.html'
        ])),
    );
});

self.addEventListener('fetch', e => {
    e.respondWith(
        caches.match(e.request).then(r => r || fetch(e.request)),
    );
});