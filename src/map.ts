import {
	GeolocateControl,
	LngLatBounds,
	Map,
	NavigationControl,
	Popup,
	FullscreenControl,
	ScaleControl
} from 'maplibre-gl';
import { stravaAttribution } from './strava'
import { lengthKm } from './gis'
const basemapStyleID = localStorage.getItem('activities-style-id') || '3e1a00aeae81496587988075fe529f71'
const styleUrl = `https://www.arcgis.com/sharing/rest/content/items/${basemapStyleID}/resources/styles/root.json`;
const url = new URL(window.location.href);
var showControls = 0
var showFilter = 0
var showHeatmap = 0
var needsHeatmapLayer = true
var firstSymbolId

const typeColours = {
	Ride: '#00f',
	Run: '#93c',
	Swim: '#d81',
	Walk: '#f00',
	Hike: '#f00',
}
const typeIcons = {
	Ride: '\u{1F6B4}',
	Run: '\u{1F3C3}',
	Swim: '\u{1F3CA}',
	Walk: '\u{1F6B6}',
	Hike: '\u{1F6B6}'
}

const attributionContent = `<ul class="attrib">${stravaAttribution}<li>&copy; <a href=\"https://www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors,</li><li>Map layer by <a href=\"https://www.esri.com/\">Esri</a></li></ul>`

class ButtonControl {
	constructor({
		className = "",
		title = "",
		eventHandler = evtHndlr,
		innerText = "",
	}) {
		this._className = className;
		this._title = title;
		this._eventHandler = eventHandler;
		this._innerText = innerText
	}

	onAdd(map) {
		this._btn = document.createElement("button");
		this._btn.className = "maplibregl-ctrl-icon" + " " + this._className;
		this._btn.type = "button";
		this._btn.title = this._title;
		this._btn.onclick = this._eventHandler;
		this._btn.innerText = this._innerText;

		this._container = document.createElement("div");
		this._container.className = "maplibregl-ctrl-group maplibregl-ctrl";
		this._container.appendChild(this._btn);

		return this._container;
	}

	onRemove() {
		this._container.parentNode.removeChild(this._container);
		this._map = undefined;
	}
}

const getDateFilter = () => {
	const startDate = document.getElementById('start').value
	const endDate = document.getElementById('end').value
	const dateFilterFrom = startDate ? ['>=', ['get', 'date'], new Date(startDate) / 6e4] : true
	const dateFilterTo = endDate ? ['<', ['get', 'date'], (new Date(endDate) / 6e4) + 1440] : true
	return ['all', dateFilterFrom, dateFilterTo]
}

const createLayerMenu = (name, noLine) => {
	const menu = document.getElementById("layers")
	const sel = document.createElement('div');
	sel.id = 'lyr-menu-t_' + name
	sel.textContent = '\u2002' + name
	sel.className = 'visibleLayer'
	menu.appendChild(sel)
	if (noLine) return sel
	const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
	const line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
	line.setAttribute('x1', '0');
	line.setAttribute('y1', '6');
	line.setAttribute('x2', '50');
	line.setAttribute('y2', '6');
	line.setAttribute('stroke', typeColours[name] || '#000')
	line.setAttribute('stroke-width', '4')
	svg.setAttribute('width', '50px')
	svg.setAttribute('height', '12px')
	svg.appendChild(line)
	sel.prepend(svg)
	return sel
}

const layerIds = []

const updateHeatmap = (m) => {
	if (showHeatmap) {
		const filter = getDateFilter()
		const visibleLayers = layerIds.reduce((a, id) => {
			if (document.getElementById('lyr-menu-' + id).className === 'visibleLayer') {
				a.push(id.slice(2))
			}
			return a
		}, [])
		filter.push(['in', ['get', 'type'], ['literal', visibleLayers]])
		console.log(filter)
		m.setFilter('c_hm', filter)
	}
}

const heatmapWeight = () => {
	const weight = parseInt(document.getElementById('hms').value)
	return {
		stops: [
			[1, 0],
			[weight > 2 ? weight : 100, 1]
		]
	}
}

export const updateHeatmapScale = (m) => {
	const hms = document.getElementById('hms')
	return () => {
		const val = hms.value
		localStorage.setItem('activities-heatmap-scale', val)
		m.setPaintProperty('c_hm', 'heatmap-weight', heatmapWeight())
	}
}

export const heatmap = (m) => {
	return () => {
		const button = document.getElementById('hm')
		const hms = document.getElementById('hmc')
		if (showHeatmap ^= true) {
			hms.style.removeProperty('display')
			button.innerHTML = '\u{29DF}'
			if (needsHeatmapLayer) {
				const features = layerIds.reduce((a, id) => {
					a.push(...m.getSource(id)._data.features)
					return a
				}, [])
				const source = m.addSource('c_hm', {
					type: 'geojson',
					data: {
						type: 'FeatureCollection',
						features
					}
				})
				const lyr = m.addLayer({
					id: 'c_hm',
					type: 'heatmap',
					source: 'c_hm',
					paint: {
						'heatmap-weight': heatmapWeight()
					}
				}, firstSymbolId)
				needsHeatmapLayer = false
			} else {
				m.setLayoutProperty('c_hm', 'visibility', 'visible')
			}
			layerIds.forEach(id => {
				m.setLayoutProperty(id, 'visibility', 'none')
			})
			updateHeatmap(m)
		} else {
			layerIds.forEach(id => {
				if (document.getElementById('lyr-menu-' + id).className === 'visibleLayer') {
					m.setLayoutProperty(id, 'visibility', 'visible')
				}
			})
			m.setLayoutProperty('c_hm', 'visibility', 'none')
			hms.style.display = 'none'
			button.innerHTML = '\u{1F525}'
		}
	}
}

export const mapFetch = () => fetch(styleUrl).then(response => response.json()).then(style => {
	return fetch(style.sources.esri.url).then(response => response.json()).then(metadata => {
		style.sources.esri = {
			type: 'vector',
			scheme: 'xyz',
			tilejson: metadata.tilejson || '2.0.0',
			format: (metadata.tileInfo && metadata.tileInfo.format) || 'pbf',
			tiles: [style.sources.esri.url + '/' + metadata.tiles[0]],
			description: metadata.description,
			name: metadata.name
		}
		return style
	}).catch(() => null)
}).catch(() => null).then(style => {
	const map = new Map({
		container: 'map',
		style: style || { version: 8, sources: {}, layers: [] },
		center: [0, 52],
		zoom: 5,
		customAttribution: attributionContent
	})

	const singlePopupContent = (feature) => {
		const props = feature.properties
		const t = props.type
		const div = document.createElement('div')
		const h = document.createElement('h2')
		const d = document.createElement('div')
		const c = document.createElement('div')
		const l = document.createElement('span')
		const id = props.stravaActivityId
		const src = map.getSource(feature.source)._data.features.filter(x => Object.entries(x.properties).every(([k, v]) => (props[k] === v)))
		if (src.length === 1) {
			l.innerText = lengthKm(src[0].geometry.coordinates).toFixed(1) + 'km\u{2003}'
		}
		c.appendChild(l)
		if (id !== undefined) {
			const href = `https://www.strava.com/activities/${id}`
			const s = document.createElement('a')
			c.appendChild(s)
			s.innerText = '[ View on Strava ]'
			s.href = href
		}
		h.innerHTML = (typeIcons[t] || `${t} |`) + ' ' + props.name
		d.innerText = new Date(props.date * 6e4).toLocaleString(undefined, { dateStyle: 'full', timeStyle: 'short' })
		div.appendChild(h)
		div.appendChild(d)
		div.appendChild(c)
		return div.innerHTML
	}

	map.on('click', e => {
		const pixTol = 2
		const features = map.queryRenderedFeatures([[e.point.x - pixTol, e.point.y - pixTol],
		[e.point.x + pixTol, e.point.y + pixTol]],
			{ layers: layerIds })
		const length = features.length
		if (length) {
			const popup = new Popup().setLngLat(e.lngLat)
			features.sort((a, b) => b.properties.date - a.properties.date)
			const content = length > 1 ? ('<div style="overflow:auto;max-height:9em">' + features.map((a, i) => {
				const single = encodeURIComponent(singlePopupContent(a))
				const p = a.properties
				const d = new Date(p.date * 6e4).toLocaleDateString()
				return `<div id="popup_${i}" onclick="this.parentNode.innerHTML = decodeURIComponent('${single}')">${d} ${p.name}</div>`
			}).join('') + '</div>') : singlePopupContent(features[0])
			popup.setHTML(content).addTo(map)
		}
	})

	const labelsOnTop = localStorage.getItem('activities-labels-on-top') !== 'false'

	const controlsTL = []
	const controlsTR = [new ButtonControl({
		className: "settings",
		title: "Settings",
		innerText: "\u{2699}\u{FE0F}",
		eventHandler: () => {
			url.pathname = url.pathname.replace(/((\/index.html)|\/)?$/, '/settings')
			window.location.replace(url)
		}
	}),
	new FullscreenControl({ container: document.querySelector('body') }),
	new NavigationControl({ visualizePitch: true }),
	new ButtonControl({
		className: "fit",
		title: "Zoom to fit",
		innerText: "\u{2922}",
		eventHandler: () => {
			const extent = new LngLatBounds()
			if (showHeatmap) {
				map.getSource('c_hm')._data.features.forEach(feature => {
					// Assumes filter of the form ['all', true, true] or
					// ['all', ['>=', ['get', 'date'], ?], ['<', ['get', 'date'], ?], ['in', ['get', 'type'], ['literal', ?]]]
					const date = feature.properties.date
					const type = feature.properties.type
					const [_, startDate, endDate, t] = map.getLayer('c_hm').filter
					const visibleLayers = t[2][1]
					console.log(visibleLayers, type)
					if ((startDate === true || date >= startDate[2]) && (endDate === true || date < endDate[2]) && visibleLayers.includes(type)) {
						for (const coord of feature.geometry.coordinates) {
							extent.extend(coord)
						}
					}
				})
			} else {
				layerIds.forEach(id => {
					if (map.getLayoutProperty(id, 'visibility') !== 'none') {
						// Assumes filter of the form ['all', true, true] or
						// ['all', ['>=', ['get', 'date'], ?], ['<', ['get', 'date'], ?]]
						const [_, startDate, endDate] = map.getLayer(id).filter
						map.getSource(id)._data.features.forEach(feature => {
							const date = feature.properties.date
							if ((startDate === true || date >= startDate[2]) && (endDate === true || date < endDate[2])) {
								for (const coord of feature.geometry.coordinates) {
									extent.extend(coord)
								}
							}
						})
					}
				})
			}
			map.fitBounds(extent, { padding: showHeatmap ? 38 : 8 })
		}
	}),
	new GeolocateControl({ trackUserLocation: true }),
	new ButtonControl({
		className: "filtering",
		title: "Filter",
		innerText: "\u{1F441}\u{FE0F}",
		eventHandler: () => {
			showFilter ^= true
			document.getElementById('filtering').style.display = showFilter ? 'inline' : 'none'
		}
	})
	]
	const controlsBL = [new ScaleControl()]

	const toggleControls = () => {
		showControls ^= true
		controlsTR.forEach(c => showControls ? map.addControl(c) : map.removeControl(c))
		controlsTL.forEach(c => showControls ? map.addControl(c, 'top-left') : map.removeControl(c))
		controlsBL.forEach(c => showControls ? map.addControl(c, 'bottom-left') : map.removeControl(c))
		document.getElementsByClassName('toggle-controls')[0].title = (showControls ? 'Hide' : 'Show') + ' controls'
	}
	const controlsToggle = new ButtonControl({ className: "toggle-controls", title: "Show controls", innerText: '\u{2630}', eventHandler: toggleControls })
	const startDate = document.getElementById("start")
	const endDate = document.getElementById("end")
	const datePicker = document.getElementById("datepicker")
	const updateDate = () => {
		const dateFilter = getDateFilter()
		layerIds.forEach(id => {
			map.setFilter(id, dateFilter)
		})
		updateHeatmap(map)
	}
	datePicker.addEventListener('reset', () => {
		layerIds.forEach(id => {
			map.setFilter(id, null)
		})
		updateHeatmap(map)
	})
	map.addControl(controlsToggle)
	startDate.addEventListener('change', updateDate)
	endDate.addEventListener('change', updateDate)
	return new Promise(resolve => {
		map.on('load', () => {
			const basemapLayers = map.getStyle().layers
			if (labelsOnTop) {
				const l = basemapLayers.find(x => x.layout && 'text-font' in x.layout)
				if (l !== -1) firstSymbolId = l?.id
			}
			var visible = true
			const sel = createLayerMenu('Basemap', true)
			sel.addEventListener('click', () => {
				visible ^= true
				sel.className = visible ? 'visibleLayer' : 'hiddenLayer'
				basemapLayers.forEach(l => {
					map.setLayoutProperty(l.id, 'visibility', visible ? 'visible' : 'none')
				})
			})
			resolve(map)
		})
	})
})

async function addSource(map, name, features) {
	return map.addSource(name, {
		type: 'geojson',
		data: {
			type: 'FeatureCollection',
			features
		}
	})
}

async function appendSource(map, source, features) {
	const data = source._data
	data.features.push(...features)
	source.setData(data)
}

export async function updateLayer(map, type, features) {
	const t = 't_' + type
	const src = map.getSource(t)
	if (src === undefined) return addSource(map, t, features.map(a => {
		a.properties.type = type
		return a
	})).then(() => {
		const sel = createLayerMenu(type)
		var visible = true
		sel.addEventListener('click', () => {
			visible ^= true
			sel.className = visible ? 'visibleLayer' : 'hiddenLayer'
			map.setLayoutProperty(t, 'visibility', visible && !showHeatmap ? 'visible' : 'none')
			updateHeatmap(map)
		})
		layerIds.push(t)
		const lyr = map.addLayer({
			id: t,
			type: 'line',
			source: t,
			paint: {
				'line-color': typeColours[type] || '#000',
				'line-width': 2
			},
			filter: getDateFilter()
		}, firstSymbolId)

		return lyr
	})
	return appendSource(map, src, features)
}
