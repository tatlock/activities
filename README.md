# Activities

Activities displays line strings on a map, including activities fetched using the Strava API. Data is stored locally on the device.

## Setup

1. Fork this repository
    * If forked to a new Gitlab repository, Activities Map will then be hosted at `<namespace>.gitlab.io/<project-name>/`
    * If not forked to Gitlab, the page must be built and hosted separately
1. [Make a Strava application](https://developers.strava.com/docs/getting-started#b-how-to-create-an-account)
    * Use `<namespace>.gitlab.io/<project-name>/` for the `Website` field, e.g. <https://tatlock.gitlab.io/activities/>
    * Use `<namespace>.gitlab.io` for the `Authorization Callback Domain`, e.g. [tatlock.gitlab.io](tatlock.gitlab.io)
1. Navigate to the Settings page at `<namespace>.gitlab.io/<project-name>/settings/`, e.g. <https://tatlock.gitlab.io/activities/settings/> and provide Strava authentication details
    * The `Client ID` and `Client Secret` provided for the Strava API should entered in the corresponding fields

The application can then be viewed at `<namespace>.gitlab.io/<project-name>/`, e.g. <https://tatlock.gitlab.io/activities/>